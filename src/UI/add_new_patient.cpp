#include "add_new_patient.h"

using namespace MedDatabase::UI;

AddNewPatient::AddNewPatient( QWidget* parent ) : 
    QDialog( parent ),
    m_ui { std::make_unique< Ui::AddNewPatientWindow >() },
    m_db { DBModel::DBObject::getDBObject() }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );
    m_ui->addmisionDE->setDate( QDate::currentDate() );
}

void AddNewPatient::on_addPatientBtn_clicked()
{
    auto checker = []( QLineEdit* const edit ) -> bool
    {
        if ( edit->text().isEmpty() )
        {
            edit->setStyleSheet( "border: 1px solid red;" );
            return false;
        }
        else
            edit->setStyleSheet( "border: 1px solid grey;" );

        return true;
    };

    if ( !checker( m_ui->fNameLE ) )
        return;

    if ( !checker( m_ui->mNameLE ) )
        return;

    if ( !checker( m_ui->lNameLE ) )
        return;

    if ( !checker( m_ui->streetLE ) )
        return;

    if ( !checker( m_ui->cityLE ) )
        return;

    if ( !checker( m_ui->stateLE ) )
        return;

    DBModel::Patient patient
    {
        -1,
        DBModel::FullName( m_ui->fNameLE->text() + " " + m_ui->mNameLE->text() + " " + m_ui->lNameLE->text() ),
        m_ui->birthdayDE->date(),
        DBModel::FullAddress( m_ui->streetLE->text() + " " + m_ui->cityLE->text() + " " + m_ui->stateLE->text() ),
        m_ui->addmisionDE->date()
    };

    m_db->addNewPatient( patient );
    this->close();
}

