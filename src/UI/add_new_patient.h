#pragma once
#ifndef UI_ADD_NEW_PATIENT_H_
#define UI_ADD_NEW_PATIENT_H_

#include "ui_add_new_patient.h"

#include <memory>

#include "../db_model/db_object.h"
#include "../db_model/patient.h"

namespace MedDatabase
{
namespace UI
{
    class AddNewPatient : public QDialog
    {
        Q_OBJECT

    public:
        AddNewPatient( QWidget* parent = Q_NULLPTR );
        ~AddNewPatient() { }

    private:
        std::unique_ptr< Ui::AddNewPatientWindow > m_ui;
        DBModel::DBObject* m_db;

    private slots:
        void on_addPatientBtn_clicked();
    };
}
}
#endif // UI_ADD_NEW_PATIENT_H_
