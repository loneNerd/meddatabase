#include "discharge_date_widget.h"

using namespace MedDatabase::UI;

DischargeDateWidget::DischargeDateWidget( const QDate& admissionDate, QWidget* parent ) :
    QDialog( parent ),
    m_ui { std::make_unique< Ui::SetDischargeDateWindow >() }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );
    m_ui->dischargeDate->setDate( QDate::currentDate() );
    m_ui->dischargeDate->setMinimumDate( admissionDate );
}

void DischargeDateWidget::on_okBtn_clicked()
{
    QDialog::done( QMessageBox::StandardButton::Ok );
    close();
}

void DischargeDateWidget::on_cancelBtn_clicked()
{
    close();
}
