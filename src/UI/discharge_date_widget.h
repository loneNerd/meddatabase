#pragma once
#ifndef UI_DISCHARGE_DATE_WIDGET_H_
#define UI_DISCHARGE_DATE_WIDGET_H_

#include "ui_set_discharge_date.h"

#include <QMessageBox>

namespace MedDatabase
{
namespace UI
{
    class DischargeDateWidget : public QDialog
    {
        Q_OBJECT

    public:
        DischargeDateWidget( const QDate& admissionDate, QWidget* parent = Q_NULLPTR );
        ~DischargeDateWidget() { }

        const QDate& getSelectedDate() const { return m_selectedDate; }

    private:
        QDate m_selectedDate = QDate();
        std::unique_ptr< Ui::SetDischargeDateWindow > m_ui;

    private slots:
        void on_okBtn_clicked();
        void on_cancelBtn_clicked();
    };
}
}


#endif // UI_DISCHARGE_DATE_WIDGET_H_
