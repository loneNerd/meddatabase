#include "med_database.h"

using namespace MedDatabase::UI;

MDBMainWindow::MDBMainWindow( QWidget* parent )
    : QMainWindow( parent ),
    m_ui { std::make_unique< Ui::MedDatabaseClass >() },
    m_dbObject { DBObject::getDBObject() }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );

    m_ui->patientList->setColumnCount( 4 );
    m_ui->patientList->setHorizontalHeaderLabels( QStringList() << "Full Name"
                                                                << "Birth Date"
                                                                << "Admission Date"
                                                                << "Actions" );

    m_ui->patientList->setColumnWidth( 0, 350 );
    m_ui->patientList->setColumnWidth( 1, 100 );
    m_ui->patientList->setColumnWidth( 2, 100 );

    updatePatientList();
}

MDBMainWindow::~MDBMainWindow()
{
    if ( m_ui )
        m_ui.release();
}

void MDBMainWindow::on_newPatientBtn_clicked()
{
    AddNewPatient newPatient;
    newPatient.exec();

    updatePatientList();
}

void MDBMainWindow::showPatientInfo( const DBModel::Patient& patient )
{
    PatientInfo info;
    info.showPatientInfo( patient );
    info.exec();

    updatePatientList();
}

void MDBMainWindow::updatePatientList()
{
    m_ui->patientList->setRowCount( 0 );

    std::vector< DBModel::Patient > patients = m_dbObject->getPatients();

    for ( size_t i = 0; i < patients.size(); ++i )
    {
        m_ui->patientList->insertRow( i );
        m_ui->patientList->setItem( i, 0, new QTableWidgetItem( patients[ i ].fullName.GetFullName() ) );
        QTableWidgetItem* birth = new QTableWidgetItem( patients[ i ].birthDate.toString( "dd.MM.yyyy" ) );
        birth->setTextAlignment( Qt::AlignmentFlag::AlignCenter );
        m_ui->patientList->setItem( i, 1, birth );
        QTableWidgetItem* admission = new QTableWidgetItem( patients[ i ].admissionDate.toString( "dd.MM.yyyy" ) );
        admission->setTextAlignment( Qt::AlignmentFlag::AlignCenter );
        m_ui->patientList->setItem( i, 2, admission );

        QWidget* pWidget = new QWidget;
        QPushButton* button = new QPushButton;
        button->setFixedWidth( 100 );
        button->setText( "Show Info" );

        button->connect( button, &QPushButton::clicked, this, [ = ]
        {
            showPatientInfo( patients[ i ] );
        } );

        QHBoxLayout* pLayout = new QHBoxLayout( pWidget );
        pLayout->addWidget( button );
        pLayout->setContentsMargins( 0, 0, 0, 0 );
        pWidget->setLayout( pLayout );

        m_ui->patientList->setCellWidget( i, 3, pWidget );
    }
}
