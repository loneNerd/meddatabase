#pragma once
#ifndef UI_MED_DATABASE_H_
#define UI_MED_DATABASE_H_

#include <QtWidgets/QMainWindow>
#include <QMessageBox.h>
#include "ui_med_database.h"
#include "add_new_patient.h"
#include "patient_info.h"

#include "../db_model/db_object.h"
#include "../db_model/patient.h"

#include "memory"

namespace MedDatabase
{
namespace UI
{
    using DBModel::DBObject;
    using DBModel::FullName;

    class MDBMainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        MDBMainWindow( QWidget* parent = Q_NULLPTR );
        ~MDBMainWindow();

        void showPatientInfo( const DBModel::Patient& patient );

    private:
        void updatePatientList();

        std::unique_ptr< Ui::MedDatabaseClass > m_ui;
        DBModel::DBObject* m_dbObject;

    private slots:
        void on_newPatientBtn_clicked();
    };

}
}

#endif //UI_MED_DATABASE_H_
