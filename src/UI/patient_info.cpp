#include "patient_info.h"

using namespace MedDatabase::UI;

PatientInfo::PatientInfo( QWidget* parent ) :
    QDialog( parent ),
    m_ui { std::make_unique< Ui::PatientInfoWindow >() },
    m_db { DBModel::DBObject::getDBObject() }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );
}

void PatientInfo::showPatientInfo( const DBModel::Patient& patient )
{
    m_currentPatient = patient;

    m_ui->fNameL->setText( patient.fullName.firstName );
    m_ui->mNameL->setText( patient.fullName.middleName );
    m_ui->lNameL->setText( patient.fullName.lastName );
    m_ui->birthdayL->setText( patient.birthDate.toString( "dd.MM.yyyy" ) );
    m_ui->streetL->setText( patient.address.street );
    m_ui->cityL->setText( patient.address.city );
    m_ui->stateL->setText( patient.address.state );
    m_ui->admissionDayL->setText( patient.admissionDate.toString( "dd.MM.yyyy" ) );
}

void PatientInfo::on_showPhotoBtn_clicked()
{
    PatientPhotos photos;
    photos.showPatientPhotosAsync( m_currentPatient.id );
    photos.exec();
}

void PatientInfo::on_dischargePatientBtn_clicked()
{
    DischargeDateWidget dischargeDate( m_currentPatient.admissionDate );
    int result = dischargeDate.exec();

    if ( result == QMessageBox::StandardButton::Ok )
    {
        m_db->dischargePatient( m_currentPatient.id, dischargeDate.getSelectedDate() );
        m_ui->dischargePatientBtn->setEnabled( false );
    }
}

void PatientInfo::on_deletePatientBtn_clicked()
{
    QMessageBox msb( QMessageBox::Icon::Question,
        "Delete patient",
        "Do you want to delete user?",
        QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No );

    if ( msb.exec() == QMessageBox::StandardButton::Yes )
    {
        m_db->deletePatient( m_currentPatient.id );
        this->close();
    }
}
