#pragma once
#ifndef UI_PATIENT_INFO_H_
#define UI_PATIENT_INFO_H_

#include "ui_patient_info.h"

#include <QInputDialog>

#include <memory>

#include "patient_photos.h"
#include "discharge_date_widget.h"
#include "../db_model/db_object.h"
#include "../db_model/patient.h"

namespace MedDatabase
{
namespace UI
{
    class PatientInfo : public QDialog
    {
        Q_OBJECT

    public:
        PatientInfo( QWidget* parent = Q_NULLPTR );
        ~PatientInfo() { }

        void showPatientInfo( const DBModel::Patient& patient );

    private:
        std::unique_ptr< Ui::PatientInfoWindow > m_ui;
        DBModel::DBObject* m_db;

        DBModel::Patient m_currentPatient;

    private slots:
        void on_showPhotoBtn_clicked();
        void on_dischargePatientBtn_clicked();
        void on_deletePatientBtn_clicked();
    };
}
}
#endif // UI_PATIENT_INFO_H_
