#include "patient_photos.h"

using namespace MedDatabase::UI;

PatientPhotos::PatientPhotos( QWidget* parent ) : 
    QDialog( parent ),
    m_ui { std::make_unique< Ui::PatientPhotos >() },
    m_db { DBModel::DBObject::getDBObject() }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );
}

void PatientPhotos::reject()
{
    if ( m_recievingPhotos.isRunning() )
        m_recievingPhotos.cancel();

    clearScrollBox();

    QDialog::reject();
}

void PatientPhotos::on_addPhotoBtn_clicked()
{
    if ( m_currentId == -1 )
    {
        QMessageBox msgBox;
        msgBox.setText( "Incorrect User" );
        msgBox.exec();
        return;
    }

    QStringList files = QFileDialog::getOpenFileNames( this, "Open Images", "", "Image Files (*.jpg)" );

    if ( files.count() == 0 )
        return;

    m_ui->progressBar->setRange( 0, files.count() );
    m_ui->progressBar->setFormat( "Uploading: %v/" + QString::number( files.count() ) );
    int currentValue = 0;
    m_ui->progressBar->setValue( 0 );
    m_ui->progressBar->setVisible( true );

    connect( m_db, &DBModel::DBObject::photoUploaded, this, [ & ]( bool result )
    {
        m_ui->progressBar->setValue( ++currentValue );
    } );

    connect( m_db, &DBModel::DBObject::uploadPhotosEnd, this, [ & ]
    {
        m_ui->progressBar->setVisible( false );
        on_updateListBtn_clicked();
    } );

    m_db->uploadPhotos( m_currentId, files );
}

void PatientPhotos::on_updateListBtn_clicked()
{
    if ( m_recievingPhotos.isRunning() )
        m_recievingPhotos.cancel();

    clearScrollBox();

    m_ui->progressBar->setVisible( false );

    uploadPhotos();
}

void PatientPhotos::clearScrollBox()
{
    m_photos.clear();

    QObjectList childrens = m_ui->scrollAreaWidgetContents->children();

    for ( auto& child : childrens )
        child->deleteLater();
}

void PatientPhotos::uploadPhotos()
{
    if ( m_currentId == -1 )
    {
        QMessageBox msgBox;
        msgBox.setText( "Incorrect User" );
        msgBox.exec();
        return;
    }

    m_ui->updateListBtn->setEnabled( false );
    m_ui->addPhotoBtn->setEnabled( false );

    m_ui->progressBar->setValue( 0 );

    int photoCount = m_db->getPhotosCount( m_currentId );
    int row = 0;
    int column = 0;

    if ( photoCount < 0 )
    {
        QMessageBox msgBox;
        msgBox.setText( "Database connection failed" );
        msgBox.exec();
        reject();
        return;
    }
    else if ( photoCount == 0 )
    {
        QLabel* label = new QLabel( m_ui->scrollAreaWidgetContents );
        label->setText( "No Photos" );
        label->setFixedSize( 520, 500 );
        label->setFont( QFont( "Arial", 24 ) );
        label->setAlignment( Qt::AlignmentFlag::AlignCenter );
        label->show();

        m_ui->progressBar->setVisible( false );
        m_ui->addPhotoBtn->setEnabled( true );
        m_ui->updateListBtn->setEnabled( true );
        return;
    }

    m_ui->progressBar->setRange( 0, photoCount );
    m_ui->progressBar->setFormat( "Loaded: %v/" + QString::number( photoCount ) );
    m_ui->progressBar->setVisible( true );

    int currentHeight = photoCount / ROW_SIZE * ( PICTURE_HEIGHT + PICTURE_MARGIN + 50 );
    int height = currentHeight > MIN_SCROL_HEIGHT_SIZE ? currentHeight : MIN_SCROL_HEIGHT_SIZE;

    m_ui->scrollAreaWidgetContents->setFixedSize( SCROLL_AREA_WIDTH_SIZE, height );

    for ( int i = 0; i < photoCount; ++i )
    {
        PhotosUI photo;

        photo.root = new QWidget( m_ui->scrollAreaWidgetContents );
        photo.verticalBox = new QVBoxLayout( photo.root );

        QMovie* loader = new QMovie( ":images/loader.gif" );
        loader->resized( QSize( PICTURE_WIDTH, PICTURE_HEIGHT ) );
        loader->start();

        photo.image = new QLabel;
        photo.image->setFixedSize( PICTURE_WIDTH, PICTURE_HEIGHT );
        photo.image->setAlignment( Qt::AlignmentFlag::AlignCenter );
        photo.image->setMovie( loader );
        photo.verticalBox->addWidget( photo.image );

        photo.date = new QLabel;
        photo.date->setText( "" );
        photo.date->setFixedSize( PICTURE_WIDTH, 10 );
        photo.date->setAlignment( Qt::AlignmentFlag::AlignCenter );
        photo.verticalBox->addWidget( photo.date, 0, Qt::AlignmentFlag::AlignCenter );

        photo.buttons = new QWidget;
        photo.horizontalBox = new QHBoxLayout( photo.buttons );

        photo.openBtn = new QPushButton;
        photo.openBtn->setFixedSize( 60, 25 );
        photo.openBtn->setText( "Open" );
        photo.horizontalBox->addWidget( photo.openBtn );

        photo.deleteBtn = new QPushButton;
        photo.deleteBtn->setFixedSize( 60, 25 );
        photo.deleteBtn->setText( "Delete" );
        photo.horizontalBox->addWidget( photo.deleteBtn );

        photo.buttons->setEnabled( false );

        photo.verticalBox->addWidget( photo.buttons );

        photo.root->move( ( PICTURE_WIDTH + PICTURE_MARGIN ) * row,
                         ( PICTURE_HEIGHT + PICTURE_MARGIN + 50 ) * column );

        photo.root->setParent( m_ui->scrollAreaWidgetContents );

        photo.root->show();

        m_photos.insert( { i, photo } );

        if ( row == ROW_SIZE - 1 )
        {
            row = 0;
            ++column;
        }
        else
            ++row;
    }

    connect( m_db, &DBModel::DBObject::photoRecieved, this, &PatientPhotos::drawPicture );

    m_recievingPhotos = QtConcurrent::run( [ & ] { return m_db->getPhotos( m_currentId ); } );

    m_watcher.setFuture( m_recievingPhotos );

    connect( &m_watcher, &QFutureWatcher< std::vector< DBModel::Photo > >::finished, this, [ & ]
    {
        m_ui->updateListBtn->setEnabled( true );
        m_ui->addPhotoBtn->setEnabled( true );
    } );
}

void PatientPhotos::deletePhoto( int photoId )
{
    m_db->deletePhoto( photoId );
}

void PatientPhotos::showPatientPhotosAsync( int id )
{
    m_currentId = id;

    uploadPhotos();
}

void PatientPhotos::drawPicture( const DBModel::Photo& photo, int index )
{
    auto iter = m_photos.find( index );

    if ( iter == m_photos.cend() ||
        !iter->second.image      ||
        !iter->second.date       ||
        !iter->second.buttons )
        return;

    iter->second.image->setPixmap( photo.photo.scaled( PICTURE_WIDTH, PICTURE_HEIGHT ) );
    iter->second.date->setText( photo.date.toString( "dd.MM.yyyy hh:mm:ss" ) );
    iter->second.buttons->setEnabled( true );

    connect( iter->second.openBtn, &QPushButton::clicked, this, [ =, photo = photo.photo ]
    {
        PhotoViewer viewer;
        viewer.openPhoto( photo );
        viewer.exec();
    } );

    connect( iter->second.deleteBtn, &QPushButton::clicked, this, [ =, id = photo.patientId ]
    {
        QMessageBox msb( QMessageBox::Icon::Question, 
                        "Delete photo", 
                        "Do you want to delete photo?",
                        QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No );
        
        if ( msb.exec() == QMessageBox::StandardButton::Yes )
        {
            deletePhoto( id );
            clearScrollBox();

            m_ui->progressBar->setVisible( false );

            uploadPhotos();
        }
    } );

    m_ui->progressBar->setValue( index + 1 );

    if ( m_photos.size() == index + 1 )
        m_ui->progressBar->setVisible( false );
}
