#pragma once
#ifndef UI_PATIENT_PHOTOS_H_
#define UI_PATIENT_PHOTOS_H_

#include <QtWidgets/QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QtConcurrent>
#include <QMessageBox>
#include <QMovie>
#include <QFileDialog>

#include <memory>
#include <vector>
#include <map>

#include "ui_patient_photos.h"
#include "photo_viewer.h"
#include "../db_model/db_object.h"
#include "../db_model/photo.h"

namespace MedDatabase
{
namespace UI
{
    struct PhotosUI
    {
        QWidget*     root;
        QVBoxLayout* verticalBox;
        QLabel*      image;
        QLabel*      date;
        QWidget*     buttons;
        QHBoxLayout* horizontalBox;
        QPushButton* openBtn;
        QPushButton* deleteBtn;
    };

    class PatientPhotos : public QDialog
    {
        Q_OBJECT

    public:
        PatientPhotos( QWidget* parent = Q_NULLPTR );
        ~PatientPhotos() { }

        void showPatientPhotosAsync( int id );

    private:
        void reject() override;

        void clearScrollBox();
        void uploadPhotos();
        void deletePhoto( int photoId );

        const int ROW_SIZE = 3;
        const int SCROLL_AREA_WIDTH_SIZE = 569;
        const int MIN_SCROL_HEIGHT_SIZE = 498;
        const int PICTURE_WIDTH = 150;
        const int PICTURE_HEIGHT = 150;
        const int PICTURE_MARGIN = 15;

        int m_currentId = -1;

        std::unique_ptr< Ui::PatientPhotos > m_ui;
        DBModel::DBObject* m_db;

        QFuture< std::vector< DBModel::Photo > > m_recievingPhotos;
        QFutureWatcher< std::vector< DBModel::Photo > > m_watcher;
        std::map< int, PhotosUI > m_photos;

    private slots:
        void drawPicture( const DBModel::Photo& photo, int index );
        void on_addPhotoBtn_clicked();
        void on_updateListBtn_clicked();
    };

}
}

#endif // UI_PATIENT_PHOTOS_H_
