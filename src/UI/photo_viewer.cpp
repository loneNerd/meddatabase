#include "photo_viewer.h"

using namespace MedDatabase::UI;

PhotoViewer::PhotoViewer( QWidget* parent ) : 
    QDialog( parent ),
    m_ui{ std::make_unique< Ui::PhotoViewer >() },
    m_widget{ new CustomElements::PhotoWidget },
    m_scrollArea { std::make_unique< CustomElements::PhotoScrollArea >( this ) }
{
    m_ui->setupUi( this );

    setWindowFlag( Qt::MSWindowsFixedSizeDialogHint );
    this->setWindowFlags( Qt::Window );
    m_scrollArea->setBackgroundRole( QPalette::Dark );
    m_scrollArea->setParent( this );
    m_scrollArea->setFixedSize( this->size() );
    m_widget->setParent( m_scrollArea->widget() );
    m_scrollArea->setWidget( m_widget );
    m_scrollArea->setAlignment( Qt::AlignmentFlag::AlignCenter );
}

void PhotoViewer::openPhoto( const QPixmap& photo )
{
    m_widget->setPhoto( photo );
    m_widget->setFixedSize( photo.size() );
    m_widget->show();
}

void PhotoViewer::resizeEvent( QResizeEvent* resizeEvent )
{
    m_scrollArea->setFixedSize( this->size() );
    QWidget::resizeEvent( resizeEvent );
}
