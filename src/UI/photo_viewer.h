#pragma once
#ifndef UI_PHOTO_VIEWER_H_
#define UI_PHOTO_VIEWER_H_

#include "ui_photo_viewer.h"

#include <QPixmap>
#include <QLabel>
#include <QWheelEvent>
#include <QMessageBox>
#include <QStyle>
#include <QApplication>
#include <QScreen>

#include "custom_elements/photo_widget.h"
#include "custom_elements/photo_scroll_area.h"

namespace MedDatabase
{
namespace UI
{
    class PhotoViewer : public QDialog
    {
        Q_OBJECT

    public:
        PhotoViewer( QWidget* parent = Q_NULLPTR );
        ~PhotoViewer() { if ( m_widget ) delete m_widget; }

        void openPhoto( const QPixmap& photo );

    protected:
        void resizeEvent( QResizeEvent* resizeEvent ) override;

    private:
        std::unique_ptr< Ui::PhotoViewer > m_ui;
        std::unique_ptr< CustomElements::PhotoScrollArea > m_scrollArea;
        CustomElements::PhotoWidget* m_widget;
    };
}
}

#endif // UI_PHOTO_VIEWER_H_
