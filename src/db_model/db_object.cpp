#include "db_object.h"

using namespace MedDatabase::DBModel;

DBObject* DBObject::getDBObject()
{
    static DBObject instance;
    return &instance;
}

bool DBObject::connectToDB()
{
    QSettings settings( QString( "configs/config.ini" ), QSettings::IniFormat );
    QString conString = settings.value( "db/conStr" ).toString();
    QString dbType = settings.value( "db/dbType" ).toString();

    m_db = QSqlDatabase::addDatabase( dbType );

    if ( !m_db.isValid() )
        return false;
    
    m_db.setDatabaseName( conString );

    if ( m_db.open() )
        return true;

    return false;
}

void DBObject::closeDB()
{
    if ( m_db.isOpen() )
        m_db.close();
}

std::vector<Patient> DBObject::getPatients()
{
    std::vector< Patient > patients;

    if ( !m_db.isOpen() )
        return patients;

    QSqlQuery query = m_db.exec( "SELECT Id, FullName, BirthDate, Address, AdmissionDate, DischargeDate FROM dbo.Patients WHERE DischargeDate IS NULL ORDER BY AdmissionDate ASC" );

    while( query.next() )
    {
        Patient patient
        {
            query.value( 0 ).toInt(),
            FullName( query.value( 1 ).toString() ),
            query.value( 2 ).toDate(),
            FullAddress( query.value( 3 ).toString() ),
            query.value( 4 ).toDate(),
            query.value( 5 ).toDate()
        };

        patients.push_back( patient );
    }

    return patients;
}

std::vector< Photo > DBObject::getPhotos( int id )
{
    std::vector<Photo> photos;

    if ( !m_db.isOpen() )
        return photos;

    QString str = "SELECT Id, Picture, Date FROM dbo.PatientPicture WHERE PatientId = " + QString::number( id ) + " ORDER BY Date ASC";

    QSqlQuery query = m_db.exec( str );

    for ( size_t i = 0; query.next(); ++i )
    {
        QPixmap pixmap;
        pixmap.loadFromData( query.value( 1 ).toByteArray() );
        QDateTime date = query.value( 2 ).toDateTime();

        Photo photo
        {
            query.value( 0 ).toInt(), 
            pixmap, 
            date
        };

        photos.push_back( photo );
        emit photoRecieved( photo, i );
    }

    return photos;
}

int DBObject::getPhotosCount( int id )
{
    if ( !m_db.isOpen() )
        return -1;

    QSqlQuery query = m_db.exec( "SELECT COUNT(*) FROM dbo.PatientPicture WHERE PatientId = " + QString::number( id ) );

    if ( !query.next() )
        return -1;

    return query.value( 0 ).toInt();
}

bool DBObject::uploadPhoto( int id, const QString& path )
{
    if ( !m_db.isOpen() )
        return false;

    QString request = "INSERT INTO dbo.PatientPicture ";
    request += "VALUES ((SELECT * FROM OPENROWSET(BULK N'";
    request += path;
    request += "', SINGLE_BLOB) as T1), ?,";
    request += QString::number( id ) + ")";

    QSqlQuery query;
    query.prepare( request );
    query.bindValue( 0, QDateTime::currentDateTime() );
    query.exec();

    return true;
}

void DBObject::uploadPhotos( int id, const QStringList& files )
{
    if ( !m_db.isOpen() )
        return;

    for ( const QString& file : files )
        emit photoUploaded(uploadPhoto( id, file ) );

    emit uploadPhotosEnd();
}

void DBObject::deletePhoto( int photoId )
{
    if ( !m_db.isOpen() )
        return;

    QString request = "DELETE FROM dbo.PatientPicture WHERE Id = " + QString::number( photoId );
    m_db.exec( request );
}

void DBObject::addNewPatient( Patient& patient )
{
    if ( !m_db.isOpen() )
        return;

    QString request = "INSERT INTO dbo.Patients VALUES ('";
    request += patient.fullName.GetFullName();
    request += "', ?, '";
    request += patient.address.GetFullAddress();
    request += "', ?, NULL)";

    QSqlQuery query;
    query.prepare( request );
    query.bindValue( 0, patient.birthDate );
    query.bindValue( 1, patient.admissionDate );

    query.exec();
}

void DBObject::dischargePatient( int id, const QDate& date )
{
    if ( !m_db.isOpen() )
        return;

    QString request = "UPDATE dbo.Patients SET DischargeDate = ? WHERE Id = ?";

    QSqlQuery query;
    query.prepare( request );
    query.bindValue( 0, date );
    query.bindValue( 1, id );

    query.exec();
}

void DBObject::deletePatient( int id )
{
    if ( !m_db.isOpen() )
        return;

    QString str = "DELETE FROM dbo.PatientPicture WHERE PatientId = ?";
    
    QSqlQuery query;
    query.prepare( str );
    query.bindValue( 0, id );
    query.exec();

    str = "DELETE FROM dbo.Patients WHERE Id = ?";

    query.prepare( str );
    query.bindValue( 0, id );
    query.exec();
}
