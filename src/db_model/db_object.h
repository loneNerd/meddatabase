#pragma once
#ifndef DB_MODEL_DB_OBJECT_H_
#define DB_MODEL_DB_OBJECT_H_

#include <QSettings>
#include <QString>
#include <QtSql>

#include <QProgressBar>
#include <QDateTime>

#include <vector>
#include <functional>

#include "patient.h"
#include "photo.h"

namespace MedDatabase
{
namespace DBModel
{
    class DBObject : public QObject
    {
        Q_OBJECT

    public:
        ~DBObject() { }

        DBObject( DBObject& other ) = delete;
        DBObject( DBObject&& other ) = delete;

        void operator=( const DBObject& other ) = delete;
        void operator=( const DBObject&& other ) = delete;

        static DBObject* getDBObject();

        bool connectToDB();
        void closeDB();

        QString getLastErrorMessage() const { return m_db.lastError().databaseText(); }

        std::vector< Patient > getPatients();
        std::vector< Photo > getPhotos( int id );

        int getPhotosCount( int id );
        bool uploadPhoto( int id, const QString& file );
        void uploadPhotos(int id, const QStringList& files );
        void deletePhoto( int photoId );
        void addNewPatient( Patient& patient );
        void dischargePatient( int id, const QDate& date );
        void deletePatient( int id );

    signals:
        void photoRecieved( const Photo& photo, int index );
        void photoUploaded( bool result );
        void uploadPhotosEnd();

    private:
        DBObject() { };
        QSqlDatabase m_db;
    };

}
}

#endif //DB_MODEL_DB_OBJECT_H_
