#include "patient.h"

using namespace MedDatabase::DBModel;

FullName::FullName( const QString& name )
{
    QStringList list = name.split( " " );

    firstName = list.value( 0 );
    middleName = list.value( 1 );
    lastName = list.value( 2 );
}

FullAddress::FullAddress( const QString& address )
{
    QStringList list = address.split( ", " );

    street = list.value( 0 );
    city = list.value( 1 );
    state = list.value( 2 );
}
