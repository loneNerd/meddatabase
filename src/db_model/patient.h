#pragma once 
#ifndef DB_MODEL_PATIENT_H_
#define DB_MODEL_PATIENT_H_

#include <QString>
#include <QDate>

namespace MedDatabase
{
namespace DBModel
{
    struct FullName
    {
        FullName() = default;
        FullName( const QString& name );

        QString GetFullName() { return firstName + " " + middleName + " " + lastName; }

        QString firstName;
        QString middleName;
        QString lastName;
    };

    struct FullAddress
    {
        FullAddress() = default;
        FullAddress( const QString& address );

        QString GetFullAddress() { return street + ", " + city + ", " + state; }

        QString street;
        QString city;
        QString state;
    };

    struct Patient
    {
        int          id;
        FullName     fullName;
        QDate        birthDate;
        FullAddress  address;
        QDate        admissionDate;
        QDate        dischargeDate;
    };

}
}

#endif //DB_MODEL_PATIENT_H_