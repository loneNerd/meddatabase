#pragma once 
#ifndef DB_MODEL_PHOTO_H_
#define DB_MODEL_PHOTO_H_

#include <QPixmap>
#include <QDate>

namespace MedDatabase
{
namespace DBModel
{
    struct Photo
    {
        int patientId;
        QPixmap photo;
        QDateTime date;
    };
}
}

#endif //DB_MODEL_PHOTO_H_