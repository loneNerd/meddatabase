#include "UI/med_database.h"
#include <QtWidgets/QApplication>
#include <QSqlDatabase>

#include "db_model/db_object.h"

using namespace MedDatabase;

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );
    DBModel::DBObject* db = MedDatabase::DBModel::DBObject::getDBObject();

    QResource::registerResource( "/resources/resources.qrc" );

    int result = -1;

    if ( db && db->connectToDB() )
    {
        UI::MDBMainWindow w;
        w.show();
        result = a.exec();
        db->closeDB();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText( "Database is not available" );
        msgBox.exec();
    }

    return result;
}
