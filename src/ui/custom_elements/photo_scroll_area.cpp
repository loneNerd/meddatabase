#include "photo_scroll_area.h"

using namespace MedDatabase::UI::CustomElements;

void PhotoScrollArea::wheelEvent( QWheelEvent* event )
{
    if ( QGuiApplication::queryKeyboardModifiers() == Qt::KeyboardModifier::ControlModifier )
        return;
    
    QScrollArea::wheelEvent( event );
}
