#pragma once
#ifndef UI_CUSTOM_ELEMENTS_PHOTO_SCROLL_AREA_H_
#define UI_CUSTOM_ELEMENTS_PHOTO_SCROLL_AREA_H_

#include <QScrollArea>
#include <QWheelEvent>
#include <QGuiApplication>

namespace MedDatabase
{
namespace UI
{
namespace CustomElements
{
    class PhotoScrollArea : public QScrollArea
    {
        Q_OBJECT

    public:
        PhotoScrollArea( QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags() ) { }
        ~PhotoScrollArea() { }

    protected:
        void wheelEvent( QWheelEvent* wheelEvent ) override;
    };
}
}
}

#endif // UI_CUSTOM_ELEMENTS_PHOTO_SCROLL_AREA_H_
