#include "photo_widget.h"

using namespace MedDatabase::UI::CustomElements;

PhotoWidget::PhotoWidget( QWidget* parent, Qt::WindowFlags f ) :
    QWidget( parent ),
    m_photoArea { std::make_unique< QLabel >( this ) } { }

void PhotoWidget::setPhoto( const QPixmap& pixmap )
{
    if ( !m_photoArea )
        return;

    m_photo = pixmap;
    m_photoArea->setPixmap( m_photo );
    m_photoArea->setFixedSize( m_photo.size() );
}

void PhotoWidget::wheelEvent( QWheelEvent* event )
{
    if ( QGuiApplication::queryKeyboardModifiers() != Qt::KeyboardModifier::ControlModifier )
        return;

    if ( event->angleDelta().y() > 0 )
        m_scale *= 1.25;
    else if ( event->angleDelta().y() < 0 )
        m_scale *= 0.75;

    double width = m_photo.width() * m_scale;
    double height = m_photo.height() * m_scale;

    QPixmap pixmap = m_photo.scaled( width, height );
    m_photoArea->setPixmap( m_photo.scaled( width, height ) );
    m_photoArea->setFixedSize( width, height );
    this->setFixedSize( width, height );
}
