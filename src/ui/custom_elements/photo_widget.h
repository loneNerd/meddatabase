#pragma once
#ifndef UI_CUSTOM_ELEMENTS_PHOTO_WIDGET_H_
#define UI_CUSTOM_ELEMENTS_PHOTO_WIDGET_H_

#include <QScrollArea>
#include <QWheelEvent>
#include <QLabel>
#include <QPixmap>
#include <QGuiApplication>

#include <memory>

namespace MedDatabase
{
namespace UI
{
namespace CustomElements
{
    class PhotoWidget : public QWidget
    {
        Q_OBJECT

    public:
        PhotoWidget( QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags() );

        ~PhotoWidget() { }

        void setPhoto( const QPixmap& pixmap );

    protected:
        void wheelEvent( QWheelEvent* event );

    private:
        std::unique_ptr< QLabel > m_photoArea;
        QPixmap m_photo;
        double m_scale = 1;
    };
}
}
}

#endif // UI_CUSTOM_ELEMENTS_PHOTO_WIDGET_H_
